# Basic Service

## run basic services
```shell
docker-compose -f docker-compose.local.yml up -d
```

## stop basic services
```shell
docker-compose -f docker-compose.local.yml down
```
